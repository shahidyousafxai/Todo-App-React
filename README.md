# BASIC REACT CRUD APP
 
A simple crud application using reactjs. It's use react router, react notification, bootstrap etc.

## To Do

- [x] Create
- [x] Read
- [ ] Update
- [x] Delete
- [ ] React router
- [x] Front-end validation (HTML)
